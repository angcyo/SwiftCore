//
// Created by angcyo on 21/09/17.
//

import Foundation
import UIKit

/// 表单录音item

class FormAudioRecordTableItem: BaseFormTableItem {

    override func initItem() {
        super.initItem()
        itemLabel = "输入语音"
    }


    override func bindCell(_ cell: DslCell, _ indexPath: IndexPath) {
        super.bindCell(cell, indexPath)

        cell.cellOf(FormAudioRecordTableCell.self) {
            let cell = $0
            let wrap = $0.wrap
            /*wrap.disallowInterceptTouch = true
            wrap.onPan(bag: gestureDisposeBag, [.began, .ended, .cancelled, .changed, .recognized]) { [self] gesture in
                L.d(gesture)
                if let vc = cell.findAttachedViewController() {
                    switch gesture.state {
                    case .began:
                        _recordViewController = vc.startRecord()
                    case .cancelled, .ended:
                        _recordViewController?.finishRecord(cell)
                    default: break
                    }
                }
            }*/
        }
    }
}

class FormAudioRecordTableCell: BaseFormTableCell {

    let wrap = RecordWrapView()
    let icon = scaleImageView(R.image.icon_record_audio())

    override func initFormCell() {
        super.initFormCell()

        renderCell(wrap) {
            $0.makeGravityLeft(offset: Res.size.x, priority: .medium)
            $0.makeGravityRight(offset: Res.size.x, priority: .medium)
            $0.makeGravityTop(offset: Res.size.x, priority: .medium)
            $0.makeGravityBottom(offset: Res.size.x, priority: .medium)
            $0.makeCenter()
            $0.makeWidth(136)
            $0.makeHeight(34)

            $0.setBorder("#E6E6E6".toColor(), radii: Res.size.roundMax)
        }
        wrap.render(formLabel)
        formLabel.remakeView {
            $0.makeGravityTop()
            $0.makeGravityBottom()

            $0.makeGravityLeft(priority: .medium)
            $0.makeGravityRight(priority: .medium)
            $0.makeCenterX(offset: 20)
        }
        wrap.render(icon) {
            $0.makeRightToLeftOf(self.formLabel)
            $0.makeCenterY(self.formLabel)
            $0.makeWidthHeight(size: 18)
        }
    }
}
