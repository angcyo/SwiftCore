//
// Created by angcyo on 21/08/30.
//

import Foundation
import UIKit

///# https://github.com/swifty-iOS/RadioCheckboxButton
///pod 'MBRadioCheckboxButton'

/// 单选按钮
func mbRadioButton() -> RadioButton {
    let button = RadioButton()
    return button
}

/// 多选按钮
func mbCheckboxButton(_ title: String? = nil,
                      titleColor: UIColor = Res.text.label.color,
                      titleSize: CGFloat = Res.text.label.size,
                      _ onCheck: ((Bool) -> Bool)? = nil) -> CheckboxButton {
    let button = CheckboxButton()
    button.checkBoxColor = CheckBoxColor(
            activeColor: Res.color.colorPrimary,
            inactiveColor: Res.color.clear,
            inactiveBorderColor: Res.color.iconColor,
            checkMarkColor: Res.color.white
    )
    button.titleLabel?.numberOfLines = 0
    button.contentVerticalAlignment = .center //勾对齐方式
    //button.contentHorizontalAlignment = .center

    //button.backgroundColor = UIColor.black

    button.setTextSize(titleSize)
    button.setTextColor(titleColor)
    button.setText(title)

    //更新layout
    button.checkboxLine = CheckboxLineStyle(checkBoxHeight: titleSize - 2, checkmarkLineWidth: -1, padding: 6)

    if let onCheck = onCheck {
        button.onClick { _ in
            let selected = !button.isSelected
            if onCheck(selected) {
                button.isSelected = selected
            }
        }
    }

    return button
}