//
// Created by angcyo on 21/09/17.
//

import Foundation
import UIKit

/// 录音wrap

class RecordWrapView: BaseUIControl {

    var _recordViewController: RecordViewController? = nil

    /// 是否取消了录制
    var _cancelRecord: Bool = false

    override func initView() {
        super.initView()
        disallowInterceptTouch = true
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        if let vc = findAttachedViewController(), isEnabled {
            vc.disableInteractivePop() //禁掉全屏pop手势
            _recordViewController = vc.startRecord()
        }
    }

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesMoved(touches, with: event)

        if let touch = touches.first {
            let point = touch.location(in: touch.window)
            //L.d(touch.location(in: self))
            L.i(point)

            if point.y < UIScreen.height / 2 {
                // 在屏幕上半部
                _cancelRecord = true
                _recordViewController?.tipLabel.setText("释放取消录制")
                _recordViewController?.tipLabel.backgroundColor = Res.color.error
            } else {
                // 在屏幕下半部
                _cancelRecord = false
                _recordViewController?.tipLabel.setText("上滑取消录制")
                _recordViewController?.tipLabel.backgroundColor = Res.color.clear
            }
        }
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
    }

    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
    }

    override func touchesFinish(touches: Set<UITouch>, with event: UIEvent?, cancelled: Bool) {
        super.touchesFinish(touches: touches, with: event, cancelled: cancelled)

        _recordViewController?.finishRecord(cancel: _cancelRecord)
    }
}