//
// Created by angcyo on 21/09/17.
//

import Foundation
import UIKit
import AVFoundation


extension UIViewController {

    /// 开始录制 [url]文件地址
    func startRecord(url: URL? = nil) -> RecordViewController {
        let recordViewController = RecordViewController()
        recordViewController.configRecorderWithURL(url: url)

        let overlayTransitioningDelegate = KMOverlayTransitioningDelegate()
        transitioningDelegate = overlayTransitioningDelegate
        recordViewController.modalPresentationStyle = .custom
        recordViewController.transitioningDelegate = overlayTransitioningDelegate

        showViewController(recordViewController)
        return recordViewController
    }
}
